import QtQuick 2.0
import QtQuick.Controls 2.5

Item {
    signal daySelected()

    property int day
    property int date
    property string text
    property bool isSelected
    property bool header: false

    Rectangle {
        anchors.fill: parent
        color: {
            if (day < 0) {
                return '#a89be4'
            }
            else if ((isSelected  &&  text.text  &&  day >= 0) || mouseArea.containsMouse) {
                return '#ffa3a3'
            }
            else 'transparent'
        }
        border.color: 'transparent'
        border.width: 0.1 * radius
        opacity: {
            if (mouseArea.pressed) {
                return 0.3
            }
            else if (isSelected) {
                return 1
            }
            else if (mouseArea.containsMouse) {
                return 0.3
            }
            else {
                return 1
            }
        }
    }

    Text {
        id: text
        anchors.centerIn: parent
        text: parent.text
        font.family: "Segoe UI"
        font.pointSize: 20
        font.bold: header
    }

    MouseArea {
        id: mouseArea

        hoverEnabled: !header
        anchors.fill: parent
        enabled: text.text  &&  day >= 0

        onClicked: {
            daySelected()
        }
    }
}
