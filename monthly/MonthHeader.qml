import QtQuick 2.0
import QtQuick.Controls 2.5

Rectangle {
    anchors.bottomMargin: 500
    Button {
        width: grid.cellWidth
        height: grid.cellHeight
        anchors.verticalCenter: parent.verticalCenter
        flat: true

        background: Rectangle {
            color: parent.hovered || parent.down ? "grey" : "white"
            opacity: parent.hovered || parent.down ? 0.2 : 1
        }

        anchors.left: parent.left
        Text {
            anchors.centerIn: parent
            text: "<"
            font.family: "Segoe UI Semibold"
            font.pointSize: 24
            color: "#1e384c"
        }

        onClicked: {
            if (monthView.month === 0) {
                monthView.year--
                monthView.month = 11
            }
            else {
                monthView.month--
            }
        }
    }

    Text { // month year
        id: monthText
        anchors.centerIn: parent
        text: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN',
               'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'][month] + ' ' + year
        font.family: "Segoe UI Semibold"
        font.pointSize: 24
        color: "#1e384c"
    }

    Button {
        width: grid.cellWidth
        height: grid.cellHeight
        anchors.verticalCenter: parent.verticalCenter
        flat: true

        background: Rectangle {
            color: parent.hovered || parent.down ? "grey" : "white"
            opacity: parent.hovered || parent.down ? 0.2 : 1
        }

        anchors.right: parent.right

        Text {
            anchors.centerIn: parent
            text: ">"
            font.family: "Segoe UI Semibold"
            font.pointSize: 24
            color: "#1e384c"
        }

        onClicked: {
            if (monthView.month === 11) {
                monthView.year++
                monthView.month = 0
            }
            else {
                monthView.month++
            }
        }
    }
}
