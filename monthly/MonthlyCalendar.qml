import QtQuick 2.0
import QtQuick.Controls 2.5

ListView {
    id: monthly
 // public
    function set(date) {
        selectedDate = new Date(date)
        positionViewAtIndex((selectedDate.getFullYear()) * 12 + selectedDate.getMonth(), ListView.Center) // index from month year
    }

    signal clicked(date clickedDate)

 // private
    property date selectedDate: new Date()

    width: 400;  height: 500
    snapMode:    ListView.SnapOneItem
    orientation: Qt.Horizontal
    clip:        true

    model: 3000 * 12 // index == months since January of the year 0

    delegate: Rectangle {
        id: monthView
        property int year:      Math.floor(index / 12)
        property int month:     index % 12 // 0 January
        property int firstDay:  new Date(year, month, 1).getDay() // 0 Sunday to 6 Saturday

        width: monthly.width;  height: monthly.height
        color: "#fff"

        Column {
            MonthHeader { // month year header
                width: monthly.width;  height: monthly.height - grid.height
            }

            Row {
                id: gridHeader
                width: monthly.width;  height: 0.125 * monthly.height

                Repeater {
                    model: 7 * 1
                    delegate: MonthDay {
                        width: grid.cellWidth
                        height: grid.cellHeight
                        day: index

                        text: ['M', 'T', 'W', 'T', 'F', 'S', 'S'][index] // M-S
                        header: true
                    }
                }
            }

            Grid { // 1 month calender
                id: grid

                width: monthly.width;  height: 0.8 * monthly.height
                property real cellWidth:  width  / columns;
                property real cellHeight: height / (rows + 1) // width and height of each cell in the grid.

                columns: 7 // days
                rows:    6

                Repeater {
                    model: grid.columns * grid.rows // 49 cells per month

                    delegate: MonthDay {
                        width: grid.cellWidth
                        height: grid.cellHeight
                        day: index

                        // TODO
                        date: day - monthView.firstDay + 2

                        text: {
                            if (new Date(monthView.year, monthView.month, date).getMonth() == monthView.month)  date // 1-31
                            else ''
                        }

                        isSelected: new Date(monthView.year, monthView.month, date).toDateString() === root.selectedDate.toDateString()

                        onDaySelected: {
                            selectedDate = new Date(year, month, date)
                            monthly.clicked(selectedDate)
                        }
                    }
                }
            }
        }
    }
}
