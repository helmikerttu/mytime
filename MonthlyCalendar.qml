import QtQuick 2.0
import QtQuick.Controls 2.5

ListView {
    id: root

 // public
    function set(date) { // new Date(2019, 10 - 1, 4)
        selectedDate = new Date(date)
        positionViewAtIndex((selectedDate.getFullYear()) * 12 + selectedDate.getMonth(), ListView.Center) // index from month year
    }

    signal clicked(date date);  // onClicked: print('onClicked', date.toDateString())

 // private
    property date selectedDate: new Date()

    width: 400;  height: 400 // default size
    snapMode:    ListView.SnapOneItem
    orientation: Qt.Horizontal
    clip:        true

    model: 3000 * 12 // index == months since January of the year 0

    delegate: Item {
        id: monthView
        property int year:      Math.floor(index / 12)
        property int month:     index % 12 // 0 January
        property int firstDay:  new Date(year, month, 1).getDay() // 0 Sunday to 6 Saturday

        width: root.width;  height: root.height

        Column {
            Item { // month year header
                width: root.width;  height: root.height - grid.height

                Button {
                    width: grid.cellWidth
                    height: grid.cellHeight
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter

                    flat: true
                    background: Rectangle {
                        color: parent.hovered || parent.down ? "grey" : "white"
                        opacity: parent.hovered || parent.down ? 0.2 : 1
                    }

                    font.pixelSize: 25
                    text: "<"
                    onClicked: {
                        if (monthView.month == 0) {
                            monthView.year--
                            monthView.month = 11
                        }
                        else {
                            monthView.month--
                        }
                    }
                }

                Text { // month year
                    anchors.centerIn: parent
                    text: ['January', 'February', 'March', 'April', 'May', 'June',
                           'July', 'August', 'September', 'October', 'November', 'December'][month] + ' ' + year
                    font {pixelSize: 0.5 * grid.cellHeight}
                }

                Button {
                    width: grid.cellWidth
                    height: grid.cellHeight
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter

                    flat: true
                    background: Rectangle {
                        color: parent.hovered || parent.down ? "grey" : "white"
                        opacity: parent.hovered || parent.down ? 0.2 : 1
                    }

                    font.pixelSize: 25
                    text: ">"
                    onClicked: {
                        if (monthView.month == 11) {
                            monthView.year++
                            monthView.month = 0
                        }
                        else {
                            monthView.month++
                        }
                    }
                }

            }

            Grid { // 1 month calender
                id: grid

                width: root.width;  height: 0.875 * root.height
                property real cellWidth:  width  / columns;
                property real cellHeight: height / rows // width and height of each cell in the grid.

                columns: 7 // days
                rows:    7

                Repeater {
                    model: grid.columns * grid.rows // 49 cells per month

                    delegate: Item { // index is 0 to 48
                        property int day:  index - 7 // 0 = top left below Monday (-7 to 41)
                        property int date: day - firstDay + 2 // 1-31
                        width: grid.cellWidth;  height: grid.cellHeight

                        Rectangle {
                            anchors.fill: parent
                            color: {
                                if (day < 0) {
                                    return '#a89be4'
                                }
                                else if ((new Date(year, month, date).toDateString() == selectedDate.toDateString()  &&  text.text  &&  day >= 0) || mouseArea.containsMouse) {
                                    return '#ffa3a3'
                                }
                                else 'transparent'
                            }
                            border.color: 'transparent'
                            border.width: 0.1 * radius
                            opacity: {
                                if (mouseArea.pressed) {
                                    return 0.3
                                }
                                else if (new Date(year, month, date).toDateString() == selectedDate.toDateString()) {
                                    return 1
                                }
                                else if (mouseArea.containsMouse) {
                                    return 0.3
                                }
                                else {
                                    return 1
                                }
                            }
                        }

                        Text {
                            id: text
                            anchors.centerIn: parent
                            font.pixelSize: 0.5 * parent.height
                            text: {
                                if (day < 0) ['M', 'T', 'W', 'T', 'F', 'S', 'S'][index] // Su-Sa
                                else if (new Date(year, month, date).getMonth() == month)  date // 1-31
                                else ''
                            }
                        }

                        MouseArea {
                            id: mouseArea

                            hoverEnabled: true
                            anchors.fill: parent
                            enabled:    text.text  &&  day >= 0

                            onClicked: {
                                selectedDate = new Date(year, month, date)
                                root.clicked(selectedDate)
                            }
                        }
                    }
                }
            }
        }
    }

     // Component.onCompleted: set(new Date()) // today (otherwise Jan 0000)
}
