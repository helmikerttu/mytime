import QtQuick 2.12
import QtQuick.Window 2.12

import "./monthly"

Window {
    id: root
    width: 900
    height: 600
    visible: true
    title: qsTr("Hello World")

    property date selectedDate: new Date()

    function dateSelected(newDate) {
        selectedDate = newDate
    }

    Rectangle {
        anchors.left: parent.left
        width: 450
        height: 600
        MonthlyCalendar {
            id: monthly
            anchors.centerIn: parent
            Component.onCompleted: {
                set(root.selectedDate)
                monthly.onClicked.connect(dateSelected)
            }
        }
    }
    Rectangle {
        anchors.right: parent.right
        width: 450
        height: 600
        color: "#1e384c"
        Rectangle {
            width: 250
            height: 450
            anchors.centerIn: parent
            color: "#1e384c"
            Text {
                id: dayText
                anchors.horizontalCenter: parent.horizontalCenter
                text: ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'][root.selectedDate.getDay()]
                font.pointSize: 24
                font.family: "Segoe UI"
                color: "white"
            }
            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: dayText.bottom
                text: root.selectedDate.getDate().toString()
                font.pointSize: 24
                font.family: "Segoe UI"
                color: "white"
            }
        }

    }

}
